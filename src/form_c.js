export const typeForm = 'form';
export const typeInput = 'input';
export const typeTextarea = 'textarea';
export const typeSelect = 'select';
export const typeCheckbox = 'checkbox';
export const typeRadio = 'radio';
export const typeButton = 'button';
export const typeLabel = 'label';
export const typeOption = 'option';

function formScript() {
  var ajax_id = "ajax-script";
  var script_file = "/js/ajaxable.js";

  var form = this;

  if (!form.hasAttribute('data-ajaxable')) return;

  var loading = form.querySelector('.loading');
  var submitBtn = form.querySelector('.submit');
  var errorMsgs = [].slice.call(form.querySelectorAll('[data-error]'));

  function loadCaptcha() {
    var captcha = form.querySelector('[data-captcha]');
  
    if (captcha) {
      var api = captcha.getAttribute('data-captcha');
      var key = captcha.querySelector('[data-captcha-key]');
      var img = captcha.querySelector('img');
      fetch(api).then(response => response.json()).then(data => {
        key.value = data['key'];
        img.setAttribute('src', data['img']);
      });
    }
  }

  function initScript(){
    ajaxable(form)
      .onStart(function (params) {
        if(loading) loading.classList.remove('hidden');
        if (submitBtn) submitBtn.classList.add('invisible');
        errorMsgs.map(item => {
          item.innerText = "";
          item.classList.add('hidden');
        })
      })
      .onEnd(function(params) {
        if(loading) loading.classList.add('hidden');
        if(submitBtn) submitBtn.classList.remove('invisible');
      })
      .onResponse(function(res, params) {
        if (res.errors) {
          var error_keys = Object.keys(res.errors);
          error_keys.map(item => {
            var error_el = form.querySelector('[data-error=' + item + ']');
            if (error_el) {
              error_el.innerText = res.errors[item][0];
              error_el.classList.remove('hidden');
            }
          });

          loadCaptcha();
        }
        else {
          Swal.fire(res.message, res.text, 'success');
          form.reset();
        }

      })
      .onError(function (err, params) {
        console.log(err);

        var error_keys = Object.keys(err.errors);
        error_keys.map(item => {
          console.log(item, err.errors[item], err.errors[item][0])
          var error_el = form.querySelector('[data-error=' + item + ']');
          if (error_el) {
            error_el.innerText = err.errors[item][0];
            error_el.classList.remove('hidden');
          }
        });
        loadCaptcha();

      });
  }

  if (typeof ajaxable == "undefined") {
    var js = document.createElement('script');
    js.id = ajax_id;
    js.onload = initScript;
    js.src = script_file;
    var top_js = document.getElementsByTagName('script')[0];
    top_js.parentNode.insertBefore(js, top_js);
  }
  else {
    initScript();
  }

}

export function loadFormComponents(editor) {
  const domc = editor.DomComponents;

  const idTrait = {
    name: 'id',
  };

  const forTrait = {
    name: 'for',
  };

  const nameTrait = {
    name: 'name',
  };

  const placeholderTrait = {
    name: 'placeholder',
  };

  const valueTrait = {
    name: 'value',
  };

  const requiredTrait = {
    type: 'checkbox',
    name: 'required',
  };

  const checkedTrait = {
    type: 'checkbox',
    name: 'checked',
  };

  domc.addType(typeForm, {
    isComponent: el => el.tagName == 'FORM',

    model: {
      defaults: {
        tagName: 'form',
        droppable: ':not(form)',
        draggable: ':not(form)',
        attributes: { method: 'get' },
        traits: [
          {
            type: 'select',
            name: 'method',
            options: [
              {value: 'get', name: 'GET'},
              {value: 'post', name: 'POST'},
            ],
          },
          {
            name: 'action',
          },
          {
            type: 'checkbox',
            label: 'Ajaxable',
            name: 'data-ajaxable'
          }
        ],
        script: formScript
      },
    },

    view: {
      events: {
        submit: e => e.preventDefault(),
      }
    },
  });


  // INPUT
  domc.addType(typeInput, {
    isComponent: el => el.tagName == 'INPUT',

    model: {
      defaults: {
        tagName: 'input',
        draggable: 'form, form *',
        droppable: false,
        highlightable: false,
        attributes: { 
          type: 'text',
          class: 'w-full bg-white rounded border border-gray-300 focus:border-indigo-500 focus:ring-2 focus:ring-indigo-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out'
        },
        traits: [
          nameTrait,
          placeholderTrait,
          {
            type: 'select',
            name: 'type',
            options: [
              { value: 'text' },
              { value: 'email' },
              { value: 'password' },
              { value: 'number' },
            ]
          },
          requiredTrait
        ],
      },
    },

    extendFnView: ['updateAttributes'],
    view: {
      updateAttributes() {
        this.el.setAttribute('autocomplete', 'off');
      },
    }
  });



  domc.addType('error-message', {
    isComponent: el => el.hasAttribute && el.hasAttribute('data-error'),
    model: {
      defaults: {
        traits: ['data-error']
      }
    }
  });


  // TEXTAREA
  domc.addType(typeTextarea, {
    extend: typeInput,
    isComponent: el => el.tagName == 'TEXTAREA',

    model: {
      defaults: {
        tagName: 'textarea',
        attributes: {
          class: "w-full bg-white rounded border border-gray-300 focus:border-indigo-500 focus:ring-2 focus:ring-indigo-200 h-32 text-base outline-none text-gray-700 py-1 px-3 resize-none leading-6 transition-colors duration-200 ease-in-out"
        },
        traits: [
          nameTrait,
          placeholderTrait,
          requiredTrait
        ]
      },
    },
  });





  // OPTION
  domc.addType(typeOption, {
    isComponent: el => el.tagName == 'OPTION',

    model: {
      defaults: {
        tagName: 'option',
        layerable: false,
        droppable: false,
        draggable: false,
        highlightable: false,
      },
    },
  });

  const createOption = (value, name) => ({ type: typeOption, components: name, attributes: { value } });

  // SELECT
  domc.addType(typeSelect, {
    extend: typeInput,
    isComponent: el => el.tagName == 'SELECT',

    model: {
      defaults: {
        tagName: 'select',
        components: [
          createOption('opt1', 'Option 1'),
          createOption('opt2', 'Option 2'),
        ],
        traits: [
          nameTrait,
          {
            name: 'options',
            type: 'select-options'
          },
          requiredTrait
        ],
      },
    },

    view: {
      events: {
        mousedown: e => e.preventDefault(),
      },
    },
  });


  // CHECKBOX
  domc.addType(typeCheckbox, {
    extend: typeInput,
    isComponent: el => el.tagName == 'INPUT' && el.type == 'checkbox',

    model: {
      defaults: {
        copyable: false,
        attributes: { 
          type: 'checkbox',
          class: 'form-checkbox h-5 w-5 text-indigo-600'
        },
        traits: [
          idTrait,
          nameTrait,
          valueTrait,
          requiredTrait,
          checkedTrait
        ],
      },
    },

    view: {
      event: {
        click: (e) => e.preventDefault()
      },
      init() {
        this.listenTo(this.model, 'change:attributes:checked', this.handleChecked);
      },

      handleChecked() {
        this.el.checked = !!this.model.get('attributes').checked;
      },

    },
  });


  // RADIO
  domc.addType(typeRadio, {
    extend: typeCheckbox,
    isComponent: el => el.tagName == 'INPUT' && el.type == 'radio',

    model: {
      defaults: {
        attributes: { 
          type: 'radio',
          class: 'form-radio h-5 w-5 text-blue-600'
        },
      },
    },
  });

  domc.addType(typeButton, {
    extend: typeInput,
    isComponent: el => el.tagName == 'BUTTON',

    model: {
      defaults: {
        tagName: 'button',
        attributes: { 
          type: 'button',
          class: 'text-white bg-indigo-500 border-0 py-2 px-6 focus:outline-none hover:bg-indigo-600 rounded text-lg'
        },
        text: 'Send',
        traits: [
          {
            name: 'text',
            changeProp: true,
          }, {
            type: 'select',
            name: 'type',
            options: [
              { value: 'button' },
              { value: 'submit' },
              { value: 'reset' },
            ]
        }]
      },

      init() {
        // const comps = this.components();
        // const tChild =  comps.length === 1 && comps.models[0];
        // const chCnt = (tChild && tChild.is('textnode') && tChild.get('content')) || '';
        // const text = chCnt || this.get('text');
        // this.set({ text });
        this.on('change:text', this.__onTextChange);
        this.__onTextChange();
        // (text !== chCnt) && this.__onTextChange();
      },

      __onTextChange() {
        this.components(this.get('text'));
      },
    },

    view: {
      events: {
        click: e => e.preventDefault(),
      },
    },
  });

  // LABEL
  domc.addType(typeLabel, {
    extend: 'text',
    isComponent: el => el.tagName == 'LABEL',

    model: {
      defaults: {
        tagName: 'label',
        components: 'Label',
        traits: [forTrait],
        attributes: {
          class: 'leading-7 text-sm text-gray-600'
        }
      },
    },
  });

  domc.addType('csrf', {
    isComponent: el => el.hasAttribute && el.hasAttribute('data-csrf'),
  });

}