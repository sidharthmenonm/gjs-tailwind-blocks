import countdown from './data/basic/countdown.svg'

import {getSvgHtml} from './utils'

const coundownScript = function(){
  const countDate = new Date(this.getAttribute('data-countdown')).getTime();

  const countdownContainer = this.querySelector('#countdown-container');
  const countdownExpired = this.querySelector('#countdown-expired');

  const daySelector = this.querySelector('[data-day]');
  const hourSelector = this.querySelector('[data-hour]');
  const minutesSelector = this.querySelector('[data-minutes]');
  const secondsSelector = this.querySelector('[data-seconds]');

  //clear old interval if any exist
  var oldInterval = this.gjs_countdown_interval;
  if(oldInterval) {
    oldInterval && clearInterval(oldInterval);
  }

  //set new interval to call movetimer every 1 sec
  var interval = setInterval(moveTimer, 1000);
  this.gjs_countdown_interval = interval;
  
  function moveTimer(){
    countdownContainer.classList.remove('hidden');
    countdownExpired.classList.add('hidden');

    const now = new Date().getTime();

    const gap = countDate - now;

    // console.log(gap);

    if(gap < 0){
      callback()
    }

    const second = 1000;
    const minute = second * 60;
    const hour = minute * 60;
    const day = hour * 24;

    const textDay = Math.floor(gap / day);
    const textHour = Math.floor((gap % day) / hour);
    const textMin = Math.floor((gap % hour) / minute);
    const textSec = Math.floor((gap % minute) / second);

    daySelector.innerText = textDay.toLocaleString('en-US', {minimumIntegerDigits: 2, useGrouping:false});
    hourSelector.innerText = textHour.toLocaleString('en-US', {minimumIntegerDigits: 2, useGrouping:false});
    minutesSelector.innerText = textMin.toLocaleString('en-US', {minimumIntegerDigits: 2, useGrouping:false});
    secondsSelector.innerText = textSec.toLocaleString('en-US', {minimumIntegerDigits: 2, useGrouping:false});

  }

  function callback(){
    clearInterval(interval);
    countdownContainer.classList.add('hidden');
    countdownExpired.classList.remove('hidden');
  }

};

export function loadCountdownBlock (editor, opt = {}) {
  const c = opt;
  let bm = editor.BlockManager;
  const category = { label: 'Advanced', order: 0, open: false };
  
  const toAdd = () => true;

  toAdd('countdown') &&
    bm.add('countdown', {
      id: 'countdown',
      label: getSvgHtml(countdown),
      category: category,
      // attributes: { class: 'gjs-fonts gjs-f-text' },
      content: {
        type: 'countdown',
        attributes: {
          class: "container mx-auto py-8"
        },
        components: [
          {
            attributes: {
              id: 'countdown-expired',
              class: 'text-center text-3xl px-5 hidden'
            },
            components: [
              {
                type: 'text',
                content: 'Expired'
              }
            ]
          },
          {
            attributes: {
              id: 'countdown-container',
              class: 'flex flex-wrap justify-center items-center '
            },
            components: [
              {
                attributes: {
                  class: "bg-red-600 rounded-md shadow-md mx-5 mb-5 flex flex-col justify-center items-center text-center py-5 px-3"
                },
                components : [
                  {
                    attributes: {
                      class: "text-5xl font-bold bg-white rounded-md shadow py-5 px-3 mb-3",
                      'data-day': true
                    },
                    content: "00"
                  },
                  {
                    type: 'text',
                    attributes: {
                      class: "text-xs font-thin text-white"
                    },
                    content: 'Days'
                  }
                ]
              },

              {
                attributes: {
                  class: "bg-red-600 rounded-md shadow-md mx-5 mb-5 flex flex-col justify-center items-center text-center py-5 px-3"
                },
                components : [
                  {
                    attributes: {
                      class: "text-5xl font-bold bg-white rounded-md shadow py-5 px-3 mb-3",
                      'data-hour': true
                    },
                    content: "00"
                  },
                  {
                    type: 'text',
                    attributes: {
                      class: "text-xs font-thin text-white"
                    },
                    content: 'Hours'
                  }
                ]
              },

              {
                attributes: {
                  class: "bg-red-600 rounded-md shadow-md mx-5 mb-5 flex flex-col justify-center items-center text-center py-5 px-3"
                },
                components : [
                  {
                    attributes: {
                      class: "text-5xl font-bold bg-white rounded-md shadow py-5 px-3 mb-3",
                      'data-minutes': true
                    },
                    content: "00"
                  },
                  {
                    type: 'text',
                    attributes: {
                      class: "text-xs font-thin text-white"
                    },
                    content: 'Minutes'
                  }
                ]
              },

              {
                attributes: {
                  class: "bg-red-600 rounded-md shadow-md mx-5 mb-5 flex flex-col justify-center items-center text-center py-5 px-3"
                },
                components : [
                  {
                    attributes: {
                      class: "text-5xl font-bold bg-white rounded-md shadow py-5 px-3 mb-3",
                      'data-seconds': true
                    },
                    content: "00"
                  },
                  {
                    type: 'text',
                    attributes: {
                      class: "text-xs font-thin text-white"
                    },
                    content: 'Seconds'
                  }
                ]
              },
            ]
          }
          
        ]
      }
    });

    editor.DomComponents.addType('countdown', {
      model: {
        defaults: {
          traits: [
            'id', 'title', 
            {
              name: 'data-countdown',
              label: "Date Time",
              placeholder: "yyyy-mm-dd hh:mm",
            }
          ],
          script: coundownScript,
        },
      },
      view: {
        init() {
          console.log('init');
          this.listenTo(this.model, 'change:attributes:data-countdown', this.updateScript);
        },
      }
    })

}