function hammenuScript() {
  
  var ham = this;

  if (this.gjs_ham_menu) {
    ham.removeEventListener('click', hamMenu)
  }
  else {
    this.gjs_ham_menu = true;
    ham.addEventListener('click', hamMenu)
  }

  function hamMenu(e) {

    var ham_menu = e.target.closest('[data-ham-menu]');
    var target_menu = ham_menu.getAttribute('data-ham-menu');
    var toggle_class = ham_menu.getAttribute('data-ham-class');
    var children = ham_menu.children;

    console.log(target_menu, toggle_class, document.querySelector(target_menu).classList.contains(toggle_class));

    if(document.querySelector(target_menu).classList.contains(toggle_class))
    {
      //open
      document.querySelector(target_menu).classList.remove(toggle_class)
      ham_menu.classList.add('fixed','top-24','left-72')
      ham_menu.classList.remove('absolute', 'left-0')
      children[0].classList.add('opacity-0')
      children[1].classList.add('transform', 'rotate-45')
      children[2].classList.add('transform', '-rotate-45')
      children[3].classList.add('opacity-0')
    }
    else {
      //close
      document.querySelector(target_menu).classList.add(toggle_class)
      ham_menu.classList.remove('fixed', 'top-24', 'left-72')
      ham_menu.classList.add('absolute', 'left-0')
      children[0].classList.remove('opacity-0')
      children[1].classList.remove('transform', 'rotate-45')
      children[2].classList.remove('transform', '-rotate-45')
      children[3].classList.remove('opacity-0')
    }
    
  }

}

export function loadHamburgerBlock (editor, opt = {}) {
  const c = opt;
  let bm = editor.BlockManager;
  const category = { label: 'Header', order: 0, open: false };
  
  const toAdd = () => true;

  toAdd('ham-menu') &&
    bm.add('ham-menu', {
      id: 'ham-menu',
      label: 'Hamburger Menu',
      category: category,
      content: `<button data-ham-menu class="cursor-pointer absolute left-4 w-10 h-10 text-xl  rounded transition duration-150 hover:shadow leading-none px-2 py-1 border border-solid border-transparent rounded bg-transparent block lg:hidden outline-none focus:outline-none" type="button">
                  <span class="block absolute w-6 h-1 rounded-sm bg-gray-600 top-3 duration-500 " ></span>
                  <span class="block absolute w-6 h-1 rounded-sm bg-gray-600 duration-500 " ></span>
                  <span class="block absolute w-6 h-1 rounded-sm bg-gray-600 duration-500 " ></span>
                  <span class="block absolute w-6 h-1 rounded-sm bg-gray-600 bottom-2 duration-500 " ></span>
                </button>`
    });

    editor.DomComponents.addType('ham-menu', {
      isComponent: el => el.hasAttribute && el.hasAttribute('data-ham-menu'),
      model: {
        defaults: {
          traits: [
            'id', 'title', 
            {
              name: 'data-ham-menu',
              label: "Target",
            },
            {
              name: 'data-ham-class',
              label: "Toggle Class",
            }
          ],
          script: hammenuScript,
          attributes: {
            'data-ham-class': '-translate-x-full',
          }
        },
      },
      view: {
        init() {
          this.listenTo(this.model, 'change:attributes:data-ham-menu change:attributes:data-ham-class', this.updateScript);
        },
      }
    })

}