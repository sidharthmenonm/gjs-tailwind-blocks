function captchaScript(){
  var captcha = this;
  
  var api = captcha.getAttribute('data-captcha');
  
  var key = captcha.querySelector('[data-captcha-key]');
  var img = captcha.querySelector('img');
  
  var refresh = captcha.querySelector('[data-captcha-refresh]');
  refresh.addEventListener('click', loadImg);
  
  function loadImg(){
    fetch(api).then(response => response.json()).then(data => {
      key.value = data['key'];
      img.setAttribute('src', data['img']);
    });
  }

  loadImg();
  
}

export function loadCaptchaBlock(editor, opt = {}) {
  const c = opt;
  let bm = editor.BlockManager;
  const category = { id: 'forms', label: 'Forms', order: 0, open: false };
  
  const toAdd = () => true;

  toAdd('captcha') &&
    bm.add('captcha', {
      id: 'captcha',
      label: 'Captcha',
      category: category,
      content: `<div data-captcha="/captcha/api" class="flex p-4 items-center">
                  <img src="#" class="w-32" />
                  <div data-captcha-refresh  class="text-green-800 hover:text-green-400 cursor-pointer mx-3">
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-refresh" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                      <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                      <path d="M20 11a8.1 8.1 0 0 0 -15.5 -2m-.5 -4v4h4"></path>
                      <path d="M4 13a8.1 8.1 0 0 0 15.5 2m.5 4v-4h-4"></path>
                    </svg>
                  </div>
                  <input data-captcha-key type="hidden" name="key">
                  <div class="relative mb-4">
                    <label for="name" class="leading-7 text-sm text-gray-600">Captcha</label>
                    <input type="text" id="captcha" name="captcha" class="w-full bg-white rounded border border-gray-300 focus:border-indigo-500 focus:ring-2 focus:ring-indigo-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out">
                  </div>
                </div>`
    });

  editor.DomComponents.addType('captcha', {
    isComponent: el => el.hasAttribute && el.hasAttribute('data-captcha'),
    model: {
      defaults: {
        traits: [
          'id', 'title', 
          {
            name: 'data-captcha',
            label: "URL",
            type: 'text',
          },
        ],
        attributes: {
          'data-captcha': '/captcha/api'
        },
        script: captchaScript,
      },
    },
    view: {
      init() {
        this.listenTo(this.model, 'change:attributes:data-captcha', this.updateScript);
      },
    }
  })
  
}