
export function loadIframeBlock(editor, opt = {}) {
  const c = opt;
  let bm = editor.BlockManager;
  const category = { label: 'Advanced', order: 0, open: false };
  
  const toAdd = () => true;

  toAdd('iframe-embed') &&
    bm.add('iframe-embed', {
      id: 'iframe-embed',
      label: 'Iframe embed',
      category: category,
      content: `<div class="w-full h-96" data-iframe ></div>`
    });
  
  editor.DomComponents.addType('iframe-embed', {
    isComponent: el => el.hasAttribute && el.hasAttribute('data-iframe'),
    model: {
      defaults: {
        traits: [
          'id',
          {
            name: 'iframe',
            label: "URL",
            changeProp: true,
          },
        ]
      },
      init() {
        this.on('change:iframe', this.__UrlChanged);
        var url = this.get('iframe');
        if (url) {
          this.__UrlChanged();
        }
      },
      __UrlChanged() {
        var url = this.get('iframe');
        var html = `<object data="${url}" type="application/pdf" width="100%" height="100%">
                      <iframe src="${url}" width="100%" height="100%" style="border: none;">
                      This browser does not support PDFs. Please download the PDF to view it: 
                      <a href="${url}">Download PDF</a>
                      </iframe>
                    </object>`
        this.components(html);
      }
    },
    view: {
      events: {
        click: e => e.preventDefault(),
      },
    },
  });
  
};