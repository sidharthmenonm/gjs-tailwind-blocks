const appendTailwindCss = (newEditor, tailwindCssUrl) => {
  const iframe = newEditor.Canvas.getFrameEl()

  if (!iframe) return

  appendCSS(newEditor, tailwindCssUrl);

  const cssStyle = document.createElement('style')
  cssStyle.type = 'text/css'
  cssStyle.innerHTML = `img.object-cover { filter: sepia(1) hue-rotate(190deg) opacity(.46) grayscale(.7) !important; }`
  iframe.contentDocument.head.appendChild(cssStyle)
}

const appendCSS = (newEditor, url) => {
  const iframe = newEditor.Canvas.getFrameEl()

  if (!iframe) return

  const cssLink = document.createElement('link')
  cssLink.href = url
  cssLink.rel = 'stylesheet'
  // cssLink.onload = () => setCssLoaded(true)
  iframe.contentDocument.head.appendChild(cssLink)

}

export { appendTailwindCss, appendCSS }