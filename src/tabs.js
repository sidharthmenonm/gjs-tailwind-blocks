function tabsScript(){
  var tabs = this;

  var tab_headings = [].slice.call(tabs.querySelectorAll('[data-tab-heading]'));
  var tab_contents = [].slice.call(tabs.querySelectorAll('[data-tab-content]'));

  function TabFn(e){
    var target_el = e.target.closest('[data-tab-heading]');
    var target = target_el.getAttribute('data-tab-heading');
    
    var active_border = tabs.getAttribute('data-active-border');
    var active_bg = tabs.getAttribute('data-active-bg');
    var active_text = tabs.getAttribute('data-active-text');

    if(target){
      //clear active
      tab_headings.map(heading => heading.classList.remove(active_border, active_bg, active_text))
      tab_contents.map(content => content.classList.add('hidden'));
      
      //set new active
      target_el.classList.add(active_border, active_bg, active_text);
      tabs.querySelector('[data-tab-content="'+target+'"]').classList.remove('hidden');
      
    }
    
  }

  tab_headings.map(tab=>{
    tab.addEventListener('click', TabFn);
  })
}

export function loadTabsBlock (editor, opt = {}) {
  const c = opt;
  let bm = editor.BlockManager;
  const category = { label: 'Advanced', order: 0, open: false };
  
  const toAdd = () => true;

  toAdd('tabs-vertical') &&
    bm.add('tabs-vertical', {
      id: 'tabs-vertical',
      label: 'Tabs',
      category: category,
      content: `<div data-tabs class="flex flex-col sm:flex-row">
                  <div class="sm:w-1/4 p-3">
                    <div data-tab-heading="one" class="bg-white p-2 shadow mb-1 cursor-pointer border-b-4 border-blue-400 bg-gray-100 text-blue-400">
                      <span>Tab 1</span>
                    </div>
                    <div data-tab-heading="two" class="bg-white p-2 shadow mb-1 cursor-pointer border-b-4">
                      <span>Tab 2</span>
                    </div>
                  </div>
                  <div class="sm:w-3/4 p-3">
                    <div data-tab-content="one" class="bg-white shadow p-3 h-full">
                      <span>
                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                      </span>
                    </div>
                    <div data-tab-content="two" class="bg-white shadow p-3 h-full hidden">
                      <span>
                        Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page
                      </span>
                    </div>
                  </div>
                </div>`,

    });

  editor.DomComponents.addType('tabs', {
    isComponent: el => el.hasAttribute && el.hasAttribute('data-tabs'),
    model: {
      defaults: {
        script: tabsScript,
        traits: [
          'id', 'title', 
          {
            name: 'data-active-text',
            label: "Active Text Color",
          },
          {
            name: 'data-active-bg',
            label: "Active BG Color",
          },
          {
            name: 'data-active-border',
            label: "Active Border Color",
          }
        ],
        attributes: {
          "data-active-text": 'text-blue-400',
          "data-active-bg": 'bg-gray-100',
          "data-active-border": 'border-blue-400'
        }
      },
    },
    view: {
      init() {
        this.listenTo(this.model, 'component:update', this.updateScript);
      },
    }
  })

  editor.DomComponents.addType('tab-heading', {
    isComponent: el => el.hasAttribute && el.hasAttribute('data-tab-heading'),
    model: {
      defaults: {
        traits: [
          'id', 'title', 
          {
            name: 'data-tab-heading',
            label: "Target",
          }
        ]
      }
    }
  })

  editor.DomComponents.addType('tab-content', {
    isComponent: el => el.hasAttribute && el.hasAttribute('data-tab-content'),
    model: {
      defaults: {
        traits: [
          'id', 'title', 
          {
            name: 'data-tab-content',
            label: "Tab Name",
          }
        ]
      }
    }
  })

}