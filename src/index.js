import {loadBasicBlocks} from './basic'
import {loadFormBlocks} from './form'
import {loadFormComponents} from './form_c';
import {loadFormTraits} from './form_t';
import {loadTailwindBlocks} from './tailwind'
import {loadCountdownBlock} from './countdown'
import {loadSplideBlock} from './swiper'
import {loadYoutubeBlock} from './youtube_bg'
import {appendTailwindCss} from './utils/appendCss';
import {loadTabsBlock} from './tabs';
import {loadShapeDividerBlock} from './shapeDivider';
import {loadChartBlock} from './charts';
import {loadAccordionBlock} from './accordion';
import {loadAosBlock} from './aos';
import {loadCountupBlock} from './countup';
import { loadIcon } from './icon';
import { loadHamburgerBlock } from './hamburger';
import { loadIframeBlock } from './iframe';
import { loadApiRepeatableBlock } from './api_repeatable';
import { loadKeralaMapBlock } from './kerala-map';
import { loadFilterBlock } from './filterable';
import { loadModalBlock } from './modal';
import { loadCaptchaBlock } from './captcha';
import { loadEmbedBlock } from './embed';

export default function(editor) {
  loadBasicBlocks(editor)
  loadCountdownBlock(editor)
  loadSplideBlock(editor);
  loadYoutubeBlock(editor);
  loadTabsBlock(editor);
  loadFormBlocks(editor);
  loadFormComponents(editor);
  loadFormTraits(editor);
  loadTailwindBlocks(editor);
  loadShapeDividerBlock(editor);
  loadChartBlock(editor);
  loadAccordionBlock(editor);
  loadAosBlock(editor);
  loadCountupBlock(editor);
  loadIcon(editor);
  loadHamburgerBlock(editor);
  loadIframeBlock(editor);
  loadApiRepeatableBlock(editor);
  loadKeralaMapBlock(editor);
  loadFilterBlock(editor);
  loadModalBlock(editor);
  loadCaptchaBlock(editor);
  loadEmbedBlock(editor);
}

