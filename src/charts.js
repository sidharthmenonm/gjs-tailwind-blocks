function chartScript(){
  var chart_id = "chart-script";
  var script_file = "https://cdnjs.cloudflare.com/ajax/libs/chartist/0.11.4/chartist.min.js";

  oldChart = this.has_chart;
  if(oldChart){
    oldChart && oldChart.detach();
  }

  const generateChart = () => {
    var chart  = this;

    var labels = chart.getAttribute('data-labels');
    var series = chart.getAttribute('data-series');
    var legends = chart.getAttribute('data-legends');
    var chart_type = chart.getAttribute('data-chart');
    var width = chart.getAttribute('data-width');
    var height = chart.getAttribute('data-height');

    // console.log(labels, series, chart_type, chart, chart.id);

    if(labels){
      labels = labels.split('|');
      if(labels.length > 1){
        labels = labels.map(item=>item.split(','));
      }
      else{
        labels = labels[0].split(',');
      }
    }

    if(series){
      series = series.split('|');
      if(series.length > 1){
        series = series.map(item=>item.split(','));
      }
      else{
        series = series[0].split(',');
      }
    }

    if(legends){
      legends = legends.split('|');
      if(legends.length > 1){
        legends = legends.map(item=>item.split(','));
      }
      else{
        legends = legends[0].split(',');
      }
    }

    var data = {
      labels: labels,
      series: series
    }

    var options = {
      width: width,
      height: height,
      plugins: [
        Chartist.plugins.legend({
            legendNames: legends,
        })
      ]
    }

    if(chart_type=="line"){
      var chart_el = new Chartist.Line('#'+chart.id, data, options)
    }
    else if(chart_type=="bar"){
      var chart_el = new Chartist.Bar('#'+chart.id, data, options)
    }
    else if(chart_type=="pie"){
      var chart_el = new Chartist.Pie('#'+chart.id, data, options)
    }

    chart.has_chart = chart_el;
  }

  const loadScript = (url, load=false) => {
    const script = document.createElement("script");
    
    if(load) script.onload = load;

    script.src = url;
    var top_js = document.getElementsByTagName('script')[0];
    top_js.parentNode.insertBefore(script, top_js);
  }

  const loadPlugin = () => {
    loadScript("https://cdnjs.cloudflare.com/ajax/libs/chartist-plugin-legend/0.6.2/chartist-plugin-legend.min.js", generateChart);
  }

  if(typeof Chartist === "undefined"){
    loadScript("https://cdnjs.cloudflare.com/ajax/libs/chartist/0.11.4/chartist.min.js", loadPlugin);
  }
  else{
    generateChart();
  }

}

export function loadChartBlock (editor, opt = {}) {
  const c = opt;
  let bm = editor.BlockManager;
  const category = { label: 'Advanced', order: 0, open: false };
  
  const toAdd = () => true;

  toAdd('chart') &&
    bm.add('chart', {
      id: 'chart',
      label: 'chart',
      category: category,
      content: '<div data-chart="bar" class="p-5"></div>',
    });


  editor.DomComponents.addType('chart', {
    isComponent: el => el.hasAttribute && el.hasAttribute('data-chart'),
    model: {
      defaults: {
        traits: [
          'id', 'title', 
          {
            name: 'data-chart',
            label: "Type",
            type: 'select',
            options: ['line', 'bar', 'pie']
          },
          {
            name: 'data-series',
            label: "Series",
            type: 'textarea'
          },
          {
            name: 'data-labels',
            label: "Label",
            type: 'textarea'
          },
          {
            name: 'data-legends',
            label: "Legends",
            type: 'textarea'
          },
          {
            name: 'data-width',
            label: 'Width'
          },
          {
            name: 'data-height',
            label: 'Height'
          }
        ],
        attributes: {
          'data-chart': 'line',
          'data-width': '100%',
          'data-height': '100%'
        },
        script: chartScript,
      },
    },
    view: {
      init() {
        console.log('init');
        this.listenTo(this.model, 'change:attributes:data-legends change:attributes:data-height change:attributes:data-width change:attributes:data-chart change:attributes:data-series change:attributes:data-labels', this.updateScript);
      },
    }
  })


}
