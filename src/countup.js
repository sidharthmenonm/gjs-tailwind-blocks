function countupScript(){
  window.currentScrollPos = window.scrollY;

  function isVisible (ele) {
    const { top, bottom } = ele.getBoundingClientRect();
    const vHeight = (window.innerHeight || document.documentElement.clientHeight);

    return (
      (top > 0 || bottom > 0) &&
      top < vHeight
    );
  }

  const generateCountUps = () => {
    var countups = [].slice.call(document.querySelectorAll("[data-countup]"));

    countups.map((countup) => {

      var updateCount = () => {
        var max = +countup.getAttribute("data-countup");
        var speed = +countup.getAttribute("data-speed");

        var count = +countup.innerText;

        var inc = Math.ceil(max / speed);

        // console.log(countup, max, speed, count, inc);

        if (count < max) {
          countup.innerText = count + inc;
          countup.active = true;
          setTimeout(updateCount, 1);
        } else {
          countup.innerText = max;
          countup.active = false
        }
      };
      if(isVisible(countup) && !countup.active){
        updateCount();
      }
    });
    
    document.addEventListener('scroll', function(){
      var scrollTop = window.scrollY
      var diff = Math.abs(scrollTop - window.currentScrollPos)
      
      if(diff>100){
        window.currentScrollPos = scrollTop;    
        generateCountUps();
      }
    })
    
  }

  generateCountUps();
  
}

export function loadCountupBlock (editor, opt = {}) {
  const c = opt;
  let bm = editor.BlockManager;
  const category = { label: 'Advanced', order: 0, open: false };
  
  const toAdd = () => true;

  toAdd('countup') &&
    bm.add('countup', {
      id: 'countup',
      label: 'Countup',
      category: category,
      content: `<div data-countup="2000" data-speed="200" class="text-8xl font-bold">0</div>`
    });

    editor.DomComponents.addType('countup', {
      isComponent: el => el.hasAttribute && el.hasAttribute('data-countup'),
      model: {
        defaults: {
          traits: [
            'id', 'title', 
            {
              name: 'data-countup',
              label: "Value",
              type: 'number'
            },
            {
              name: 'data-speed',
              label: "Speed",
              type: 'number'
            }
          ],
          script: countupScript,
          attributes: {
            'data-speed': 200,
            'data-countup': 2000
          }
        },
      },
      view: {
        init() {
          console.log('init');
          this.listenTo(this.model, 'change:attributes:data-countup change:attributes:data-speed', this.updateScript);
        },
      }
    })

}