function embedScript() {
  var block = this;

  var script_id = "embed_script_"+block.id;
  var script_file = block.getAttribute('data-embed');

  //check if script already exist
  while (block.firstChild) {
    block.firstChild.remove()
  }

  var attributes = [].slice.call(block.attributes).filter(item => item.name.includes('data-x-'));

  var js = document.createElement('script');
  js.id = script_id;
  js.src = script_file;

  attributes.map(item => {
    js.setAttribute(item.name, item.value);
  });

  block.append(js);

}

export function loadEmbedBlock(editor, opt = {}) {
  const c = opt;
  let bm = editor.BlockManager;
  const category = { label: 'Advanced', order: 0, open: false };
  
  const toAdd = () => true;

  toAdd('embed') &&
    bm.add('embed', {
      id: 'embed',
      label: 'Embed',
      category: category,
      content: `<div data-embed class="p-5"></div>`
    });

  editor.DomComponents.addType('embed', {
    isComponent: el => el.hasAttribute && el.hasAttribute('data-embed'),
    model: {
      defaults: {
        script: embedScript,
        traits: [
          'id',
          {
            name: 'data-embed',
            label: "URL",
          },
        ]
      }
    },
    view: {
      init() {
        this.listenTo(this.model, 'change:attributes:data-embed', this.updateScript);
      },
    }
  });
}