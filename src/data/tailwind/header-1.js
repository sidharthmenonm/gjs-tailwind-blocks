const source = `
<header class="w-72 min-h-screen md:w-full md:min-h-0 md:h-20 p-8 md:py-2 bg-indigo-900 left-0 top-0 
 md:flex md:items-center md:justify-between
 shadow-lg ease-in-out transition-transform transition-medium transform duration-500 -translate-x-full md:translate-x-0 fixed z-10
 md:bg-transparent text-white md:text-black">
  <div class="container mx-auto flex flex-wrap  flex-col md:flex-row items-center">
    <a class="flex title-font font-medium items-center text-gray-900 mb-4 md:mb-0">
      <span data-icon class="inline-block">
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-10 h-10 text-white p-2 bg-indigo-500 rounded-full" viewBox="0 0 24 24">
          <path d="M12 2L2 7l10 5 10-5-10-5zM2 17l10 5 10-5M2 12l10 5 10-5"></path>
        </svg>
      </span>
      <span class="ml-3 text-xl">Tailblocks</span>
    </a>
    <nav class="md:ml-auto flex flex-wrap items-center text-base md:justify-center">
      <a class="mr-5 hover:text-gray-900 p-2">First Link</a>
      <a class="mr-5 hover:text-gray-900 p-2">Second Link</a>
      <a class="mr-5 hover:text-gray-900 p-2">Third Link</a>
      <a class="mr-5 hover:text-gray-900 p-2">Fourth Link</a>
    </nav>
    <a class="inline-flex items-center bg-gray-100 border-0 py-1 px-3 focus:outline-none hover:bg-gray-200 rounded text-base mt-4 md:mt-0">Button
      <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-4 h-4 ml-1" viewBox="0 0 24 24">
        <path d="M5 12h14M12 5l7 7-7 7"></path>
      </svg>
    </a>
  </div>
</header>
`
export { source }
