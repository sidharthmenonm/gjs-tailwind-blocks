function modalScript(){
  var modal_container = this;
  
  var modal_button = modal_container.querySelector('[data-modal-button]');
  
  modal_button.addEventListener('click', LaunchModal);
  
  function loadCaptcha() {
    var captcha = modal_container.querySelector('[data-captcha]');
  
    if (captcha) {
      var api = captcha.getAttribute('data-captcha');
      var key = captcha.querySelector('[data-captcha-key]');
      var img = captcha.querySelector('img');
      fetch(api).then(response => response.json()).then(data => {
        key.value = data['key'];
        img.setAttribute('src', data['img']);
      });
    }
  }

  function LaunchModal(e){
    var target = e.target.closest('[data-modal]');
    var modal = target.querySelector('[data-modal-content]');
    modal.classList.remove('hidden');
    loadCaptcha();
  }
  
  var close_buttons = [].slice.call(modal_container.querySelectorAll('.modal-close'))
  
  close_buttons.map((button)=>{
    button.addEventListener('click', CloseModal)
  })
  
  function CloseModal(e){
    var target = e.target.closest('[data-modal]');
    var modal = target.querySelector('[data-modal-content]');
    modal.classList.add('hidden');
  }
  
}

export function loadModalBlock(editor, opt = {}) {
  const c = opt;
  let bm = editor.BlockManager;
  const category = { label: 'Advanced', order: 0, open: false };
  
  const toAdd = () => true;

  toAdd('modal') &&
    bm.add('modal', {
      id: 'modal',
      label: 'Modal',
      category: category,
      content: `<div data-modal>

                  <button data-modal-button class="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-red-600 text-base font-medium text-white hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500 sm:ml-3 sm:w-auto sm:text-sm"><span>Launch Modal</span></button>
                  
                  <div data-modal-content class="fixed z-10 inset-0 overflow-y-auto hidden" aria-labelledby="modal-title" role="dialog" aria-modal="true">
                    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
                      
                      <div class="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" aria-hidden="true"></div>
                  
                      <span class="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>
                  
                      <form class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full">
                        <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                          <div class="sm:flex sm:items-start">
                            <div class="mx-auto flex-shrink-0 flex items-center justify-center h-12 w-12 rounded-full bg-red-100 sm:mx-0 sm:h-10 sm:w-10">
                              <!-- Heroicon name: outline/exclamation -->
                              <svg class="h-6 w-6 text-red-600" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z" />
                              </svg>
                            </div>
                            <div class="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
                              <div class="text-lg leading-6 font-medium text-gray-900" id="modal-title">
                                Deactivate account
                              </div>
                              <div class="mt-2">
                                <div class="text-sm text-gray-500">
                                  Are you sure you want to deactivate your account? All of your data will be permanently removed. This action cannot be undone.
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                          <button type="button" class="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-red-600 text-base font-medium text-white hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500 sm:ml-3 sm:w-auto sm:text-sm">
                            <span>Deactivate</span>
                          </button>
                          <button type="button" class="modal-close mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm">
                            <span>Cancel</span>
                          </button>
                          <div class="loading mr-2 text-green-400 hidden">
                            <svg class="animate-spin -ml-1 mr-3 h-5 w-5 text-blue-700" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
                              <circle class="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" stroke-width="4"></circle>
                              <path class="opacity-75" fill="currentColor" d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"></path>
                            </svg>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>`
    });
  
  editor.DomComponents.addType('modal', {
    isComponent: el => el.hasAttribute && el.hasAttribute('data-modal'),
    model: {
      defaults: {
        script: modalScript,
      },
    },
    view: {
      init() {
        this.listenTo(this.model, 'change', this.updateScript);
      },
    }
  })

  editor.DomComponents.addType('modal-button', {
    isComponent: el => el.hasAttribute && el.hasAttribute('data-modal-button'),
    model: {
      defaults: {
        traits: [
          {
            name: 'text',
            changeProp: true,
          }
        ]
      },
      init() {
        this.on('change:text', this.__onTextChange);
      },
      __onTextChange() {
        this.components(this.get('text'));
      },
    }
  })

  editor.DomComponents.addType('modal-content', {
    isComponent: el => el.hasAttribute && el.hasAttribute('data-modal-content'),
  })
      
}