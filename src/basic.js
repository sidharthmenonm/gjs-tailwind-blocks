import b1s from './data/basic/column1 1.svg'
import b2s from './data/basic/column2 1.svg'
import b3s from './data/basic/column3 1.svg'
// import b4s from './data/basic/column3-7 1.svg'
import b5s from './data/basic/image 1.svg'
import b6s from './data/basic/link 1.svg'
import b7s from './data/basic/map 1.svg'
import b8s from './data/basic/text 1.svg'
import b9s from './data/basic/video.svg'

import {getSvgHtml} from './utils'

export function loadBasicBlocks (editor, opt = {}) {
    const c = opt;
    let bm = editor.BlockManager;
    const category = { label: 'Basic', order: 0, open: true };
    
    const toAdd = () => true // blocks.indexOf(name) >= 0;
  
    toAdd('section') &&
      bm.add('section', {
        label: getSvgHtml(b1s) + 'section',
        category: category,
        // attributes: { class: 'gjs-fonts gjs-f-text' },
        content: "<section class='p-5 bg-gray-100'></section>"
      });
    
    toAdd('container') &&
      bm.add('container', {
        label: getSvgHtml(b1s) + 'container',
        category: category,
        // attributes: { class: 'gjs-fonts gjs-f-text' },
        content: "<div class='p-5 bg-white container mx-auto'></div>"
      });

    toAdd('col-2') &&
      bm.add('col-2', {
        label: getSvgHtml(b2s),
        category: category,
        // attributes: { class: 'gjs-fonts gjs-f-text' },
        content: `<div class="flex flex-wrap -mx-2">
                    <div class="w-full sm:w-1/2 px-2 mb-4 sm:mb-0">
                      <div class="bg-gray-200 p-5"></div>
                    </div>
                    <div class="w-full sm:w-1/2 px-2 mb-4 sm:mb-0">
                      <div class="bg-gray-200 p-5"></div>
                    </div>
                  </div>`
      }); 
      
    toAdd('col-3') &&
      bm.add('col-3', {
        label: getSvgHtml(b3s),
        category: category,
        // attributes: { class: 'gjs-fonts gjs-f-text' },
        content: `<div class="flex flex-wrap -mx-2">
                    <div class="w-full sm:w-1/3 px-2 mb-4 sm:mb-0">
                      <div class="bg-gray-200 p-5"></div>
                    </div>
                    <div class="w-full sm:w-1/3 px-2 mb-4 sm:mb-0">
                      <div class="bg-gray-200 p-5"></div>
                    </div>
                    <div class="w-full sm:w-1/3 px-2 mb-4 sm:mb-0">
                      <div class="bg-gray-200 p-5"></div>
                    </div>
                  </div>`
      });  

    toAdd('text') &&
      bm.add('text', {
        label: getSvgHtml(b8s),
        tagName: 'span',
        category: category,
        // attributes: { class: 'gjs-fonts gjs-f-text' },
        content: {
          type: 'text',
          content: 'Insert your styled text here',
          // content: 'Insert your text here',
          activeOnRender: 1
        }
      });

    // toAdd('link') &&
    //   bm.add('link', {
    //     label: getSvgHtml(b6s),
    //     category: category,
    //     content: {
    //       type: 'link',
    //       content: 'link',
    //       style: { color: '#6366f1' },
    //     }
    //   });

    toAdd('button') &&
      bm.add('button', {
        label: getSvgHtml(b6s),
        category: category,
        droppable: true,
        content: `<a class="inline-flex items-center bg-gray-100 border-0 py-1 px-3 focus:outline-none hover:bg-gray-200 rounded text-base mt-4 md:mt-0">
                    <span>Button</span>
                    <span data-icon class="inline-block">
                      <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-4 h-4 ml-1" viewBox="0 0 24 24">
                        <path d="M5 12h14M12 5l7 7-7 7"></path>
                      </svg>
                    </span>
                  </a>`
        // content: {
        //   tagName: 'a',
        //   type: 'link',
        //   attributes: {
        //     class: 'w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-green-600 text-base font-medium text-white hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500 sm:ml-3 sm:w-auto sm:text-sm'
        //   },
        //   // traits: ['href', 'target', 'title', 'id'],
        //   components: [
        //     {
        //       type: 'text',
        //       content: 'Link'
        //     }
        //   ]
        // }

      });

    toAdd('image') &&
      bm.add('image', {
        label: getSvgHtml(b5s),
        category: category,
        // attributes: { class: 'gjs-fonts gjs-f-image' },
        content: {
          type: 'image',
          activeOnRender: 1
        }
      });
  
    toAdd('video') &&
      bm.add('video', {
        label: getSvgHtml(b9s),
        category: category,
        // attributes: { class: 'fa fa-youtube-play' },
        content: {
          type: 'video',
          src: 'img/video2.webm',
          style: {
            height: '100%',
            width: '100%'
          }
        }
      });
  
    toAdd('map') &&
      bm.add('map', {
        label: getSvgHtml(b7s),
        category: category,
        // attributes: { class: 'fa fa-map-o' },
        content: {
          type: 'map',
          style: { height: '350px' }
        }
      });


  editor.DomComponents.addType('link', {
    isComponent: el => el.tagName && el.tagName == 'A',
    model: {
      defaults: {
        droppable: true,
        traits: [
          'id', 'title', 
          {
            type: 'text',
            name: 'href',
            label: 'URL',
            placeholder: 'Insert Link',
          },
          {
            type: 'select',
            name: 'target',
            label: 'Target',
            options: [ // Array of options
              { id: '_self', name: 'Same Window'},
              { id: '_blank', name: 'New Window'},
            ]
          }
        ]
      }
    }
  });
  
}