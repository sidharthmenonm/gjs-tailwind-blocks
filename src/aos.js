function aosScript(){

  const initAos = () => {
    AOS.init();
  }

  const loadScript = (url, load=false) => {
    const script = document.createElement("script");
    if(load) script.onload = initAos;
    script.src = url;
    var top_js = document.getElementsByTagName('script')[0];
    top_js.parentNode.insertBefore(script, top_js);
  }

  if(typeof AOS === "undefined"){
    loadScript('https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.js', true);
  }
  else{
    AOS.init();
  }

}

export function loadAosBlock (editor, opt = {}) {
  const c = opt;
  let bm = editor.BlockManager;
  const category = { label: 'Advanced', order: 0, open: false };
  
  const toAdd = () => true;

  toAdd('aos') &&
    bm.add('aos', {
      id: 'aos',
      label: 'AOS',
      category: category,
      content: '<div data-aos class="p-3"></div>'

    });

  editor.DomComponents.addType('aos', {
    isComponent: el => el.hasAttribute && el.hasAttribute('data-aos'),
    model: {
      defaults: {
        traits: [
          'id', 'title', 
          {
            name: 'data-aos',
            label: "Animation",
            type: 'select',
            options: [
              'fade','fade-up','fade-down','fade-left','fade-right','fade-up-right','fade-up-left','fade-down-right','fade-down-left',
              'flip-up','flip-down','flip-left','flip-right',
              'slide-up','slide-down','slide-left','slide-right',
              'zoom-in','zoom-in-up','zoom-in-down','zoom-in-left','zoom-in-right','zoom-out','zoom-out-up','zoom-out-down','zoom-out-left','zoom-out-right'
            ]
          },
          {
            name: 'data-aos-easing',
            label: "Easing",
            type: 'select',
            options: [
              'linear','ease','ease-in','ease-out','ease-in-out','ease-in-back','ease-out-back','ease-in-out-back','ease-in-sine','ease-out-sine','ease-in-out-sine','ease-in-quad','ease-out-quad','ease-in-out-quad','ease-in-cubic','ease-out-cubic','ease-in-out-cubic','ease-in-quart','ease-out-quart','ease-in-out-quart'
            ]
          },
          {
            name: 'data-aos-offset',
            label: "Offset",
            type: 'number'
          },
          {
            name: 'data-aos-duration',
            label: "Duration",
            type: 'number'
          },
          {
            name: 'data-aos-delay',
            label: "Delay",
            type: 'number'
          },
          {
            name: 'data-aos-anchor',
            label: "Anchor",
          },
          {
            name: 'data-aos-anchor-placement',
            label: "Anchor Placement",
            type: 'select',
            options: [
              'top-bottom','top-center','top-top','center-bottom','center-center','center-top','bottom-bottom','bottom-center','bottom-top'
            ]
          },
          {
            name: 'data-aos-once',
            label: 'Once',
            type: 'checkbox',
          }
        ],
        attributes: {
          'data-aos-offset': '120',
          'data-aos-duration': '400',
          'data-aos-delay': '0',
        },
        script: aosScript,
      },
    },
    view: {
      init() {
        console.log('init');
        this.listenTo(this.model, 'change:attributes', this.updateScript);
      },
    }
  })

}
