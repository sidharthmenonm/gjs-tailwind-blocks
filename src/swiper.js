import slider from './data/basic/slider.svg'

import {getSvgHtml} from './utils'

const splideScript = function(){
  var splide_id = "splide-script";
  var script_file = "/js/splide.js";

  var splide = this;

  oldSplide = this.gjs_splide_el;
  if(oldSplide){
    oldSplide && oldSplide.destroy();
  }

  var options = {
    type : splide.getAttribute('data-splide-type'),
    rewind : splide.hasAttribute('data-splide-rewind'),//?splide.getAttribute('data-splide-rewind'):false,
    speed : splide.getAttribute('data-splide-speed'),
    perPage : splide.getAttribute('data-splide-per-page'),
    gap : splide.getAttribute('data-splide-gap'),
    autoplay : splide.hasAttribute('data-splide-autoplay'),//?splide.getAttribute('data-splide-autoplay'):false,
    interval : splide.getAttribute('data-splide-interval'),
    arrows : splide.hasAttribute('data-splide-arrows'),//?splide.getAttribute('data-splide-arrows'):false,
    pagination : splide.hasAttribute('data-splide-pagination'),//?splide.getAttribute('data-splide-pagination'):false,
    breakpoints: {
      480: {
        perPage: splide.getAttribute('data-splide-per-page-mobile'),
      },
      640: {
        perPage: splide.getAttribute('data-splide-per-page-tablet'),
      },
    }
  }

  function initScript(){
    console.log('init slider');
    var el = new Splide('#'+splide.id, options).mount();
    splide.gjs_splide_el = el; 
  }

  if (typeof Splide == "undefined") {
    var js = document.createElement('script');
    js.id = splide_id;
    js.onload = initScript;
    js.src = script_file;
    var top_js = document.getElementsByTagName('script')[0];
    top_js.parentNode.insertBefore(js, top_js);
  }
  else {
    initScript();
  }

}

export function loadSplideBlock (editor, opt = {}) {
  const c = opt;
  let bm = editor.BlockManager;
  const category = { label: 'Advanced', order: 0, open: false };
  
  const toAdd = () => true;

  toAdd('splide') &&
    bm.add('splide', {
      id: 'splide',
      label: getSvgHtml(slider),
      category: category,
      // attributes: { class: 'gjs-fonts gjs-f-text' },
      content: {
        type: 'splide',
        attributes: {
          class: "splide",
          "data-splide" : true
        },
        droppable: false,
        components: [
          {
            attributes: {
              class: 'splide__track'
            },
            droppable: false,
            components:[
              {
                attributes: {
                  class: 'splide__list'
                },
                droppable: true,
                components: [
                  {
                    attributes: {
                      class: 'splide__slide p-5 bg-gray-300 h-56'
                    },
                    components: [
                      {
                        attributes: {
                          class: "p-5 h-full bg-white flex items-center justify-center text-5xl font-black text-yellow-700",
                        },
                        components:[
                          {
                            type: 'text',
                            content: 'Hello World'
                          }
                        ]
                      }
                    ]
                  },
                  {
                    attributes: {
                      class: 'splide__slide p-5 bg-gray-400 h-56'
                    },
                    components: [
                      {
                        attributes: {
                          class: "p-5 h-full bg-white flex items-center justify-center text-5xl font-black text-yellow-700",
                        },
                        components:[
                          {
                            type: 'text',
                            content: 'Hello World'
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      }
    });


  editor.DomComponents.addType('splide', {
    isComponent: el => el.hasAttribute && el.hasAttribute('data-splide'),
    model: {
      defaults: {
        traits: [
          'id', 'title', 
          {
            name: 'data-splide-type',
            label: "Type",
            type: 'select',
            options: ['slide', 'loop', 'fade']
          },
          {
            name: 'data-splide-rewind',
            label: "Rewind",
            type: 'checkbox'
          },
          {
            name: 'data-splide-speed',
            label: 'Speed',
            type: 'number'
          },
          {
            name: 'data-splide-per-page',
            label: 'Per Page',
            type: 'number'
          },
          {
            name: 'data-splide-per-page-tablet',
            label: 'Tablet',
            type: 'number'
          },
          {
            name: 'data-splide-per-page-mobile',
            label: 'Mobile',
            type: 'number'
          },
          {
            name: 'data-splide-gap',
            label: 'Gap',
            type: 'text'
          },
          {
            name: 'data-splide-autoplay',
            label: "Autoplay",
            type: 'checkbox'
          },
          {
            name: 'data-splide-interval',
            label: 'Interval',
            type: 'number'
          },
          {
            name: 'data-splide-arrows',
            label: "Arrows",
            type: 'checkbox'
          },
          {
            name: 'data-splide-pagination',
            label: "Pagination",
            type: 'checkbox'
          },
        ],
        attributes: {
          'data-splide-type': 'slide',
          'data-splide-speed': 400,
          'data-splide-per-page': 1,
          'data-splide-per-page-tablet': 1,
          'data-splide-per-page-mobile': 1,
          'data-splide-gap': 0,
          'data-splide-interval': 5000,
          'data-splide-arrows': true,
          'data-splide-pagination': true,
        },
        script: splideScript,
      },
    },
    view: {
      init() {
        this.listenTo(this.model, 
          'change:attributes:data-splide-pagination change:attributes:data-splide-arrows change:attributes:data-splide-interval change:attributes:data-splide-autoplay change:attributes:data-splide-gap change:attributes:data-splide-per-page change:attributes:data-splide-per-page-tablet change:attributes:data-splide-per-page-mobile change:attributes:data-splide-speed change:attributes:data-splide-type change:attributes:data-splide-rewind', 
          this.updateScript);
      },
    }
  })

}