function filterScript(){
  var filter_block = this;

  var filter_buttons = [].slice.call(filter_block.querySelectorAll('[data-filter-button]'));

  filter_buttons.map((button) => {
    var color = button.getAttribute('data-filter-color');
    button.classList.add('border-' + color, 'hover:bg-' + color, 'hover:text-white');

    button.removeEventListener('click', FilterItems);
    button.addEventListener('click', FilterItems);
  });

  function FilterItems(e) {
    var target = e.target.closest('[data-filter-button]');
    var filter = target.getAttribute('data-filter-button');

    var filter_items = [].slice.call(filter_block.querySelectorAll('[data-filter-item]'));

    if(filter){
      filter_items.map(item => {
        if(item.classList.contains(filter)){
          item.classList.remove('hidden');
        }
        else{
          item.classList.add('hidden');
        }
      })
    }
    else{
      filter_items.map(item => {
        item.classList.remove('hidden');
      })
    }
    
    filter_buttons.map(button => {
      button.classList.remove('text-white', 'bg-'+button.getAttribute('data-filter-color'))
    })
    
    target.classList.add('text-white', 'bg-'+target.getAttribute('data-filter-color'))
    
  }
}

export function loadFilterBlock(editor, opt = {}) {
  const c = opt;
  let bm = editor.BlockManager;
  const category = { label: 'Advanced', order: 0, open: false };
  
  const toAdd = () => true;

  toAdd('filterable') &&
    bm.add('filterable', {
      id: 'filterable',
      label: 'Filter',
      category: category,
      content: `<div data-filter class="p-5">
                  <div class="flex mb-5">
                    <button data-filter-button data-filter-color="yellow-600" class="border border-dashed px-5 py-1 text-sm rounded-full mr-2 bg-yellow-600 text-white">All</button>
                    <button data-filter-button="cloud" data-filter-color="blue-600" class="border border-dashed px-5 py-1 text-sm rounded-full mr-2"><span>Cloud</span></button>
                    <button data-filter-button="payment" data-filter-color="green-600" class="border border-dashed px-5 py-1 text-sm rounded-full mr-2"><span>Payment Gateway</span></button>
                  </div>
                  <div class="grid md:grid-cols-3 grid-cols-1 gap-5">
                    <div data-filter-item class="cloud bg-white shadow-md p-5">
                      <span>Cloud</span>
                    </div>
                    <div data-filter-item class="payment bg-white shadow-md p-5">
                      <span>Payment</span>
                    </div>
                  </div>
                </div>`

    });

  
  editor.DomComponents.addType('filter', {
    isComponent: el => el.hasAttribute && el.hasAttribute('data-filter'),
    model: {
      defaults: {
        script: filterScript,
      },
    },
    view: {
      init() {
        this.listenTo(this.model, 'change', this.updateScript);
      },
    }
  })

  editor.DomComponents.addType('filter-button', {
    isComponent: el => el.hasAttribute && el.hasAttribute('data-filter-button'),
    model: {
      defaults: {
        traits: [
          'id', 'title', 
          {
            name: 'data-filter-button',
            label: "Filter",
            type: 'text'
          },
          {
            name: 'data-filter-color',
            label: "Color",
            type: 'text'
          },

        ],
        attributes: {
          'data-filter-button': '',
          'data-filter-color': 'yellow-600'
        }
      },
    },
    view: {
      init() {
        this.listenTo(this.model, 'change:attributes:data-filter-button change:attributes:data-filter-color', this.updateScript);
      },
    }
  })

  editor.DomComponents.addType('filter-item', {
    isComponent: el => el.hasAttribute && el.hasAttribute('data-filter-item'),
    view: {
      init() {
        this.listenTo(this.model, 'change:attributes', this.updateScript);
      },
    }
  })

}
      