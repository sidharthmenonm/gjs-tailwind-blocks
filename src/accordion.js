
export function loadAccordionBlock (editor, opt = {}) {
  const c = opt;
  let bm = editor.BlockManager;
  const category = { label: 'Advanced', order: 0, open: false };
  
  const toAdd = () => true;

  toAdd('accordion-checkbox') &&
    bm.add('accordion-checkbox', {
      id: 'accordion-checkbox',
      label: 'Accordion 1',
      category: category,
      content: `<div data-accordion>
                  <div data-accordion-item>
                    <input type="checkbox" id="chck1" name="one">
                    <label for="chck1" data-accordion-label class="flex justify-between p-3 border-b border-secondary text-secondary text-white transition duration-300 cursor-pointer"><span>Item 1</span></label>
                    <div data-accordion-content>
                      <span>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ipsum, reiciendis!</span>
                    </div>
                  </div>
                  <div data-accordion-item>
                    <input type="checkbox" id="chck2" name="one">
                    <label for="chck2" data-accordion-label class="flex justify-between p-3 border-b border-secondary text-secondary text-white transition duration-300 cursor-pointer"><span>Item 1</span></label>
                    <div data-accordion-content>
                      <span>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ipsum, reiciendis!</span>
                    </div>
                  </div>
                </div>`

    });

    toAdd('accordion-radio') &&
      bm.add('accordion-radio', {
        id: 'accordion-radio',
        label: 'Accordion 2',
        category: category,
        content: `<div data-accordion>
                    <div data-accordion-item>
                      <input type="radio" id="chck1" name="one" class="absolute opacity-0">
                      <label for="chck1" data-accordion-label class="flex justify-between p-3 border-b border-secondary text-secondary text-white transition duration-300 cursor-pointer"><span>Item 1</span></label>
                      <div data-accordion-content>
                        <span>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ipsum, reiciendis!</span>
                      </div>
                    </div>
                    <div data-accordion-item>
                      <input type="radio" id="chck2" name="one" class="absolute opacity-0">
                      <label for="chck2" data-accordion-label class="flex justify-between p-3 border-b border-secondary text-secondary text-white transition duration-300 cursor-pointer"><span>Item 1</span></label>
                      <div data-accordion-content>
                        <span>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ipsum, reiciendis!</span>
                      </div>
                    </div>
                  </div>`

      });


      editor.DomComponents.addType('accordion', {
        isComponent: el => el.hasAttribute && el.hasAttribute('data-accordion'),
      });

      editor.DomComponents.addType('accordion-item', {
        isComponent: el => el.hasAttribute && el.hasAttribute('data-accordion-item'),
      });

      editor.DomComponents.addType('accordion-label', {
        isComponent: el => el.hasAttribute && el.hasAttribute('data-accordion-label'),
        model: {
          defaults: {
            traits: ['for'],
          }
        }
      });

      editor.DomComponents.addType('accordion-content', {
        isComponent: el => el.hasAttribute && el.hasAttribute('data-accordion-content'),
      });

}
