

export function loadIcon(editor, opt = {}) {
  const c = opt;
  let bm = editor.BlockManager;
  const category = { label: 'Basic', order: 0, open: false };
  
  const toAdd = () => true;

  toAdd('icon') &&
    bm.add('icon', {
      id: 'icon',
      label: 'Icon',
      category: category,
      content: `<span data-icon class="inline-block"></span>`
    });
  
  toAdd('ksum-logo') &&
    bm.add('ksum-logo', {
      id: 'ksum-logo',
      label: 'KSUM Logo',
      category: category,
      content: `
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                        width="165px" height="92px" viewBox="0 0 165 92" enable-background="new 0 0 165 92" xml:space="preserve">
                    <g>
                      <g>
                        <polygon fill="#00AEEF" points="65.471,26.929 56.1,32.387 54.611,24.449 63.652,17.118 		"/>
                        <polygon fill="#00AEEF" points="83.386,15.96 74.346,21.142 72.252,9.125 81.291,1.793 		"/>
                        <polygon fill="#00AEEF" points="95.183,26.324 85.481,29.63 83.386,16.015 92.646,10.944 		"/>
                        <polygon fill="#00AEEF" points="107.365,40.435 97.276,41.372 95.072,25.606 104.828,23.292 		"/>
                        <polygon fill="#00AEEF" points="88.017,42.75 78.259,44.679 76.385,32.883 85.812,28.914 		"/>
                        <polygon fill="#00AEEF" points="99.978,56.75 89.892,56.475 87.851,42.639 97.606,40.986 		"/>
                        <polygon fill="#00AEEF" points="76.44,32.991 67.124,36.739 65.416,26.819 74.511,21.029 		"/>
                        <polygon fill="#00AEEF" points="68.999,46.771 59.297,48.537 57.643,40.599 67.18,36.905 		"/>
                        <polygon fill="#00AEEF" points="80.409,56.639 70.597,56.639 68.833,46.607 78.425,44.733 		"/>
                        <polygon fill="#00AEEF" points="60.951,56.529 50.974,56.088 49.54,49.805 59.242,48.371 		"/>
                        <polygon fill="#00AEEF" points="57.918,40.599 48.437,43.961 46.839,37.346 56.155,32.44 		"/>
                        <polygon fill="#00AEEF" points="49.761,50.08 39.949,50.686 38.736,45.946 48.382,43.906 		"/>
                      </g>
                      <g>
                        <path fill="#0B9444" d="M30.701,62.564h2.189v4.319h0.043c0.218-0.377,0.45-0.725,0.667-1.073l2.218-3.246h2.711l-3.233,4.16
                          l3.407,5.609h-2.58l-2.392-4.219l-0.841,1.029v3.188h-2.189V62.564z"/>
                        <path fill="#0B9444" d="M53.131,68.203h-3.595v2.318h4.015v1.813h-6.233v-9.771h6.03v1.813h-3.812v2.028h3.595V68.203z"/>
                        <path fill="#0B9444" d="M62.775,62.693c0.71-0.115,1.769-0.202,2.943-0.202c1.45,0,2.464,0.218,3.16,0.769
                          c0.58,0.465,0.898,1.145,0.898,2.044c0,1.246-0.884,2.103-1.725,2.406v0.043c0.681,0.274,1.058,0.929,1.305,1.827
                          c0.304,1.102,0.608,2.377,0.797,2.754h-2.261c-0.16-0.275-0.391-1.072-0.681-2.275c-0.261-1.218-0.682-1.552-1.58-1.565h-0.667
                          v3.842h-2.189V62.693z M64.964,66.898h0.87c1.102,0,1.754-0.551,1.754-1.406c0-0.899-0.609-1.35-1.624-1.363
                          c-0.537,0-0.841,0.044-1,0.073V66.898z"/>
                        <path fill="#0B9444" d="M81.493,69.826l-0.695,2.508h-2.291l2.986-9.771h2.898l3.029,9.771h-2.377l-0.754-2.508H81.493z
                            M83.972,68.174l-0.607-2.072c-0.174-0.58-0.348-1.306-0.493-1.885h-0.028c-0.146,0.579-0.289,1.318-0.449,1.885l-0.58,2.072
                          H83.972z"/>
                        <path fill="#0B9444" d="M96.444,62.564h2.218v7.914h3.885v1.855h-6.103V62.564z"/>
                        <path fill="#0B9444" d="M113.931,69.826l-0.695,2.508h-2.291l2.986-9.771h2.898l3.029,9.771h-2.377l-0.754-2.508H113.931z
                            M116.408,68.174l-0.608-2.072c-0.174-0.58-0.348-1.306-0.492-1.885h-0.029c-0.146,0.579-0.289,1.318-0.449,1.885l-0.58,2.072
                          H116.408z"/>
                      </g>
                      <g>
                        <g>
                          <path fill="#231F20" d="M0.583,88.842c0.771,0.395,1.955,0.789,3.177,0.789c1.316,0,2.011-0.545,2.011-1.372
                            c0-0.79-0.602-1.241-2.124-1.786c-2.105-0.731-3.478-1.897-3.478-3.74c0-2.162,1.805-3.815,4.793-3.815
                            c1.429,0,2.481,0.301,3.233,0.64l-0.639,2.313c-0.508-0.244-1.41-0.602-2.65-0.602c-1.241,0-1.843,0.563-1.843,1.221
                            c0,0.81,0.715,1.166,2.35,1.787c2.237,0.826,3.29,1.992,3.29,3.777c0,2.124-1.635,3.93-5.113,3.93
                            c-1.447,0-2.876-0.375-3.59-0.771L0.583,88.842z"/>
                          <path fill="#231F20" d="M12.874,81.529H9.471v-2.406h9.738v2.406H15.75v10.264h-2.876V81.529z"/>
                          <path fill="#231F20" d="M22.12,88.541l-0.903,3.252h-2.97l3.872-12.67h3.76l3.93,12.67h-3.083l-0.978-3.252H22.12z
                              M25.334,86.396l-0.79-2.688c-0.226-0.752-0.451-1.691-0.639-2.443h-0.038c-0.188,0.752-0.376,1.711-0.583,2.443l-0.752,2.688
                            H25.334z"/>
                          <path fill="#231F20" d="M31.289,79.292c0.921-0.149,2.293-0.265,3.816-0.265c1.88,0,3.195,0.283,4.098,0.998
                            c0.752,0.602,1.166,1.483,1.166,2.649c0,1.616-1.147,2.726-2.237,3.12v0.057c0.884,0.357,1.372,1.203,1.692,2.368
                            c0.395,1.431,0.79,3.084,1.034,3.572h-2.933c-0.207-0.356-0.507-1.392-0.883-2.951c-0.338-1.579-0.883-2.012-2.049-2.03h-0.864
                            v4.982h-2.84V79.292z M34.127,84.743h1.128c1.429,0,2.274-0.714,2.274-1.823c0-1.166-0.79-1.748-2.105-1.768
                            c-0.695,0-1.09,0.057-1.297,0.095V84.743z"/>
                          <path fill="#231F20" d="M44.669,81.529h-3.403v-2.406h9.738v2.406h-3.459v10.264h-2.876V81.529z"/>
                          <path fill="#231F20" d="M55.173,79.123v7.293c0,2.182,0.827,3.29,2.293,3.29c1.504,0,2.331-1.054,2.331-3.29v-7.293h2.857v7.105
                            c0,3.91-1.974,5.771-5.283,5.771c-3.195,0-5.075-1.769-5.075-5.81v-7.066H55.173z"/>
                          <path fill="#231F20" d="M64.925,79.292c0.883-0.149,2.125-0.265,3.873-0.265c1.767,0,3.026,0.34,3.873,1.017
                            c0.808,0.64,1.354,1.69,1.354,2.933c0,1.24-0.414,2.293-1.166,3.008c-0.978,0.922-2.425,1.334-4.117,1.334
                            c-0.376,0-0.714-0.019-0.978-0.056v4.53h-2.838L64.925,79.292L64.925,79.292z M67.763,85.044
                            c0.245,0.058,0.545,0.075,0.959,0.075c1.522,0,2.463-0.771,2.463-2.067c0-1.166-0.809-1.86-2.237-1.86
                            c-0.583,0-0.978,0.057-1.185,0.111V85.044z"/>
                          <path fill="#231F20" d="M89.94,86.943c-0.058-1.523-0.113-3.365-0.113-5.209h-0.057c-0.396,1.617-0.92,3.423-1.41,4.908
                            l-1.541,4.942h-2.236l-1.354-4.906c-0.414-1.485-0.847-3.29-1.147-4.944h-0.038c-0.074,1.711-0.131,3.666-0.227,5.246
                            l-0.223,4.813h-2.652l0.809-12.67h3.815l1.24,4.229c0.396,1.467,0.791,3.045,1.072,4.53h0.057
                            c0.357-1.467,0.789-3.14,1.203-4.549l1.354-4.211h3.739l0.695,12.671h-2.801L89.94,86.943z"/>
                          <path fill="#231F20" d="M97.792,79.123v12.67h-2.877v-12.67H97.792z"/>
                          <path fill="#231F20" d="M100.159,88.842c0.77,0.395,1.955,0.789,3.176,0.789c1.316,0,2.012-0.545,2.012-1.372
                            c0-0.79-0.602-1.241-2.125-1.786c-2.104-0.731-3.477-1.897-3.477-3.74c0-2.162,1.805-3.815,4.793-3.815
                            c1.43,0,2.482,0.301,3.234,0.64l-0.642,2.313c-0.506-0.244-1.407-0.602-2.649-0.602c-1.24,0-1.842,0.563-1.842,1.221
                            c0,0.81,0.715,1.166,2.35,1.787c2.238,0.826,3.291,1.992,3.291,3.777c0,2.124-1.637,3.93-5.113,3.93
                            c-1.449,0-2.877-0.375-3.592-0.771L100.159,88.842z"/>
                          <path fill="#231F20" d="M110.099,88.842c0.771,0.395,1.955,0.789,3.178,0.789c1.314,0,2.013-0.545,2.013-1.372
                            c0-0.79-0.603-1.241-2.125-1.786c-2.104-0.731-3.479-1.897-3.479-3.74c0-2.162,1.806-3.815,4.795-3.815
                            c1.429,0,2.48,0.301,3.232,0.64l-0.639,2.313c-0.508-0.244-1.41-0.602-2.65-0.602s-1.842,0.563-1.842,1.221
                            c0,0.81,0.713,1.166,2.35,1.787c2.236,0.826,3.289,1.992,3.289,3.777c0,2.124-1.635,3.93-5.113,3.93
                            c-1.446,0-2.877-0.375-3.59-0.771L110.099,88.842z"/>
                          <path fill="#231F20" d="M122.821,79.123v12.67h-2.877v-12.67H122.821z"/>
                          <path fill="#231F20" d="M136.692,85.326c0,4.154-2.521,6.674-6.224,6.674c-3.76,0-5.959-2.839-5.959-6.448
                            c0-3.798,2.427-6.636,6.166-6.636C134.567,78.916,136.692,81.83,136.692,85.326z M127.538,85.495
                            c0,2.481,1.164,4.229,3.082,4.229c1.938,0,3.045-1.843,3.045-4.306c0-2.274-1.09-4.229-3.063-4.229
                            C128.665,81.189,127.538,83.033,127.538,85.495z"/>
                          <path fill="#231F20" d="M138.36,91.793v-12.67h3.348l2.631,4.643c0.752,1.336,1.504,2.914,2.068,4.344h0.057
                            c-0.188-1.674-0.244-3.385-0.244-5.282v-3.704h2.631v12.67h-3.008l-2.707-4.889c-0.752-1.354-1.578-2.988-2.199-4.473
                            l-0.057,0.019c0.076,1.673,0.113,3.459,0.113,5.526v3.816H138.36z"/>
                        </g>
                      </g>
                    </g>
                    <g>
                      <circle fill="none" stroke="#000000" stroke-width="0.58" stroke-miterlimit="10" cx="156.031" cy="83.228" r="4.311"/>
                      <g>
                        <path d="M154.471,85.561v-4.295h1.904c0.383,0,0.674,0.039,0.873,0.115c0.199,0.078,0.358,0.214,0.478,0.409
                          s0.179,0.411,0.179,0.647c0,0.305-0.099,0.562-0.296,0.771s-0.502,0.342-0.914,0.398c0.15,0.072,0.265,0.144,0.343,0.214
                          c0.166,0.152,0.323,0.343,0.472,0.571l0.747,1.169h-0.715l-0.568-0.894c-0.166-0.258-0.303-0.455-0.41-0.592
                          s-0.204-0.232-0.288-0.287c-0.086-0.055-0.172-0.093-0.26-0.114c-0.064-0.014-0.17-0.021-0.316-0.021h-0.659v1.907H154.471z
                            M155.039,83.161h1.222c0.26,0,0.463-0.026,0.609-0.081c0.146-0.053,0.258-0.139,0.334-0.258c0.076-0.117,0.114-0.246,0.114-0.385
                          c0-0.203-0.074-0.37-0.222-0.501s-0.38-0.196-0.698-0.196h-1.359V83.161z"/>
                      </g>
                    </g>
                  </svg>
                `
    });

  toAdd('ksum-logo-white') &&
    bm.add('ksum-logo-white', {
      id: 'ksum-logo-white',
      label: 'KSUM Logo White',
      category: category,
      content: `
                  <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="190px"
                  height="120px" viewBox="0 0 190 120" enable-background="new 0 0 190 120" xml:space="preserve">
                    <g id="Layer_2">
                      <g>
                        <circle fill="none" stroke="#FFFFFF" stroke-width="0.58" stroke-miterlimit="10" cx="184.689" cy="101.562" r="4.311"/>
                        <g>
                          <path fill="#FFFFFF" d="M183.129,103.895V99.6h1.904c0.383,0,0.674,0.039,0.873,0.115c0.199,0.078,0.358,0.214,0.478,0.409
                            s0.179,0.411,0.179,0.647c0,0.305-0.099,0.562-0.296,0.771s-0.502,0.342-0.914,0.398c0.15,0.072,0.265,0.144,0.343,0.214
                            c0.166,0.152,0.323,0.343,0.472,0.571l0.747,1.169h-0.715l-0.568-0.894c-0.166-0.258-0.303-0.455-0.41-0.592
                            s-0.204-0.232-0.288-0.287c-0.086-0.055-0.172-0.093-0.26-0.114c-0.064-0.014-0.17-0.021-0.316-0.021h-0.659v1.907H183.129z
                              M183.697,101.495h1.222c0.26,0,0.463-0.026,0.609-0.081c0.146-0.053,0.258-0.139,0.334-0.258
                            c0.076-0.117,0.114-0.246,0.114-0.385c0-0.203-0.074-0.37-0.222-0.501s-0.38-0.196-0.698-0.196h-1.359V101.495z"/>
                        </g>
                      </g>
                    </g>
                    <g id="Layer_1">
                      <g>
                        <g>
                          <polygon fill="#00AEEF" points="81.98,37.207 71.2,43.484 69.488,34.354 79.89,25.92 			"/>
                          <polygon fill="#00AEEF" points="102.592,24.588 92.191,30.548 89.782,16.724 100.182,8.29 			"/>
                          <polygon fill="#00AEEF" points="116.161,36.51 105.001,40.314 102.592,24.652 113.244,18.817 			"/>
                          <polygon fill="#00AEEF" points="130.174,52.744 118.572,53.821 116.033,35.685 127.257,33.023 			"/>
                          <polygon fill="#00AEEF" points="107.918,55.407 96.694,57.626 94.538,44.055 105.38,39.49 			"/>
                          <polygon fill="#00AEEF" points="121.679,71.512 110.074,71.195 107.727,55.28 118.952,53.376 			"/>
                          <polygon fill="#00AEEF" points="94.602,44.18 83.884,48.493 81.918,37.081 92.381,30.418 			"/>
                          <polygon fill="#00AEEF" points="86.04,60.033 74.878,62.064 72.977,52.932 83.947,48.683 			"/>
                          <polygon fill="#00AEEF" points="99.166,71.385 87.879,71.385 85.85,59.845 96.884,57.688 			"/>
                          <polygon fill="#00AEEF" points="76.782,71.258 65.304,70.75 63.655,63.521 74.815,61.875 			"/>
                          <polygon fill="#00AEEF" points="73.293,52.932 62.387,56.8 60.548,49.19 71.264,43.546 			"/>
                          <polygon fill="#00AEEF" points="63.91,63.842 52.621,64.535 51.226,59.083 62.323,56.736 			"/>
                        </g>
                        <g>
                          <path fill="#FFFFFF" d="M41.983,78.199h2.518v4.973h0.05c0.25-0.436,0.518-0.834,0.767-1.236l2.552-3.736h3.118l-3.718,4.787
                            l3.918,6.455h-2.967l-2.753-4.854l-0.966,1.182v3.672h-2.519V78.199L41.983,78.199z"/>
                          <path fill="#FFFFFF" d="M67.786,84.688H63.65v2.668h4.618v2.084h-7.171v-11.24h6.938v2.084h-4.386v2.338h4.136L67.786,84.688
                            L67.786,84.688z"/>
                          <path fill="#FFFFFF" d="M78.881,78.352c0.816-0.137,2.035-0.234,3.386-0.234c1.667,0,2.834,0.248,3.636,0.885
                            c0.666,0.531,1.032,1.316,1.032,2.35c0,1.434-1.016,2.418-1.983,2.768v0.049c0.783,0.318,1.217,1.068,1.5,2.104
                            c0.351,1.268,0.7,2.734,0.918,3.168h-2.602c-0.184-0.316-0.45-1.234-0.784-2.617c-0.3-1.4-0.783-1.785-1.817-1.801h-0.768v4.418
                            h-2.518V78.352z M81.398,83.188h1c1.27,0,2.02-0.633,2.02-1.617c0-1.037-0.7-1.551-1.868-1.57c-0.617,0-0.967,0.051-1.151,0.084
                            V83.188z"/>
                          <path fill="#FFFFFF" d="M100.413,86.555l-0.799,2.885h-2.636l3.435-11.24h3.334l3.486,11.24h-2.732l-0.867-2.885H100.413z
                              M103.266,84.654l-0.7-2.385c-0.201-0.668-0.401-1.502-0.57-2.17h-0.029c-0.169,0.668-0.332,1.518-0.519,2.17l-0.665,2.385
                            H103.266z"/>
                          <path fill="#FFFFFF" d="M117.613,78.199h2.551v9.105h4.471v2.135h-7.021V78.199z"/>
                          <path fill="#FFFFFF" d="M137.729,86.555l-0.799,2.885h-2.637l3.436-11.24h3.337l3.482,11.24h-2.733l-0.866-2.885H137.729z
                              M140.58,84.654l-0.702-2.385c-0.199-0.668-0.399-1.502-0.567-2.17h-0.031c-0.169,0.668-0.334,1.518-0.517,2.17l-0.668,2.385
                            H140.58z"/>
                        </g>
                        <g>
                          <g>
                            <path fill="#FFFFFF" d="M7.336,108.43c0.886,0.455,2.249,0.91,3.654,0.91c1.514,0,2.313-0.627,2.313-1.582
                              c0-0.906-0.69-1.428-2.442-2.053c-2.422-0.842-4-2.184-4-4.303c0-2.49,2.076-4.391,5.514-4.391c1.644,0,2.854,0.346,3.72,0.736
                              l-0.734,2.66c-0.586-0.281-1.622-0.693-3.05-0.693s-2.119,0.648-2.119,1.404c0,0.932,0.822,1.342,2.703,2.057
                              c2.573,0.949,3.784,2.289,3.784,4.346c0,2.443-1.881,4.52-5.882,4.52c-1.666,0-3.311-0.432-4.132-0.887L7.336,108.43z"/>
                            <path fill="#FFFFFF" d="M21.475,100.018H17.56V97.25h11.204v2.768h-3.98v11.807h-3.309V100.018z"/>
                            <path fill="#FFFFFF" d="M32.11,108.084l-1.038,3.74h-3.417L32.11,97.25h4.325l4.52,14.574h-3.546l-1.125-3.74H32.11z
                                M35.81,105.617l-0.909-3.092c-0.259-0.865-0.519-1.943-0.734-2.811h-0.043c-0.217,0.867-0.434,1.969-0.67,2.811l-0.866,3.092
                              H35.81z"/>
                            <path fill="#FFFFFF" d="M42.658,97.443c1.061-0.172,2.639-0.305,4.39-0.305c2.163,0,3.676,0.328,4.714,1.15
                              c0.865,0.691,1.341,1.705,1.341,3.047c0,1.861-1.317,3.139-2.572,3.59v0.064c1.016,0.412,1.578,1.387,1.945,2.727
                              c0.455,1.645,0.908,3.547,1.19,4.107h-3.375c-0.237-0.41-0.584-1.6-1.016-3.395c-0.39-1.816-1.016-2.313-2.357-2.334h-0.994
                              v5.729h-3.266V97.443z M45.924,103.715h1.298c1.644,0,2.616-0.82,2.616-2.098c0-1.342-0.908-2.01-2.423-2.033
                              c-0.799,0-1.254,0.064-1.491,0.109V103.715z"/>
                            <path fill="#FFFFFF" d="M58.052,100.018h-3.915V97.25h11.202v2.768H61.36v11.807h-3.308L58.052,100.018L58.052,100.018z"/>
                            <path fill="#FFFFFF" d="M70.135,97.25v8.391c0,2.51,0.952,3.785,2.64,3.785c1.73,0,2.682-1.213,2.682-3.785V97.25h3.287v8.174
                              c0,4.498-2.271,6.641-6.077,6.641c-3.676,0-5.839-2.035-5.839-6.684V97.25H70.135z"/>
                            <path fill="#FFFFFF" d="M81.354,97.443c1.017-0.172,2.444-0.305,4.454-0.305c2.034,0,3.481,0.393,4.456,1.17
                              c0.929,0.734,1.556,1.947,1.556,3.375c0,1.426-0.477,2.637-1.34,3.459c-1.125,1.061-2.79,1.537-4.736,1.537
                              c-0.434,0-0.821-0.021-1.126-0.066v5.211h-3.265L81.354,97.443L81.354,97.443z M84.618,104.063
                              c0.282,0.063,0.628,0.086,1.104,0.086c1.753,0,2.834-0.889,2.834-2.379c0-1.342-0.931-2.141-2.574-2.141
                              c-0.671,0-1.125,0.063-1.363,0.131V104.063L84.618,104.063z"/>
                            <path fill="#FFFFFF" d="M110.132,106.244c-0.064-1.75-0.132-3.869-0.132-5.988h-0.063c-0.456,1.859-1.059,3.936-1.625,5.645
                              l-1.771,5.688h-2.572l-1.56-5.645c-0.475-1.709-0.971-3.785-1.315-5.688h-0.045c-0.087,1.967-0.151,4.219-0.262,6.033
                              l-0.258,5.535h-3.049L98.41,97.25h4.39l1.429,4.865c0.453,1.688,0.909,3.502,1.231,5.211h0.067
                              c0.409-1.686,0.906-3.611,1.384-5.232l1.557-4.844h4.302l0.8,14.574h-3.223L110.132,106.244z"/>
                            <path fill="#FFFFFF" d="M119.165,97.25v14.574h-3.311V97.25H119.165z"/>
                            <path fill="#FFFFFF" d="M121.888,108.43c0.885,0.455,2.248,0.91,3.652,0.91c1.515,0,2.314-0.627,2.314-1.582
                              c0-0.906-0.694-1.428-2.444-2.053c-2.424-0.842-4-2.184-4-4.303c0-2.49,2.074-4.391,5.515-4.391
                              c1.644,0,2.853,0.346,3.722,0.736l-0.739,2.66c-0.583-0.281-1.619-0.693-3.049-0.693c-1.428,0-2.118,0.648-2.118,1.404
                              c0,0.932,0.821,1.342,2.702,2.057c2.576,0.949,3.786,2.289,3.786,4.346c0,2.443-1.881,4.52-5.884,4.52
                              c-1.667,0-3.308-0.432-4.132-0.887L121.888,108.43z"/>
                            <path fill="#FFFFFF" d="M133.32,108.43c0.888,0.455,2.248,0.91,3.657,0.91c1.51,0,2.314-0.627,2.314-1.582
                              c0-0.906-0.692-1.428-2.446-2.053c-2.423-0.842-4-2.184-4-4.303c0-2.49,2.073-4.391,5.515-4.391
                              c1.644,0,2.852,0.346,3.718,0.736l-0.734,2.66c-0.583-0.281-1.622-0.693-3.051-0.693c-1.424,0-2.116,0.648-2.116,1.404
                              c0,0.932,0.82,1.342,2.702,2.057c2.573,0.949,3.783,2.289,3.783,4.346c0,2.443-1.88,4.52-5.883,4.52
                              c-1.662,0-3.309-0.432-4.128-0.887L133.32,108.43z"/>
                            <path fill="#FFFFFF" d="M147.956,97.25v14.574h-3.309V97.25H147.956z"/>
                            <path fill="#FFFFFF" d="M163.914,104.385c0,4.781-2.896,7.68-7.158,7.68c-4.326,0-6.854-3.266-6.854-7.418
                              c0-4.371,2.791-7.635,7.093-7.635C161.471,97.012,163.914,100.363,163.914,104.385z M153.383,104.582
                              c0,2.852,1.338,4.865,3.545,4.865c2.231,0,3.504-2.119,3.504-4.953c0-2.615-1.254-4.865-3.523-4.865
                              C154.68,99.629,153.383,101.748,153.383,104.582z"/>
                            <path fill="#FFFFFF" d="M165.835,111.824V97.25h3.849l3.026,5.34c0.865,1.537,1.732,3.354,2.381,4.996h0.064
                              c-0.215-1.924-0.282-3.891-0.282-6.076v-4.26h3.028v14.574h-3.459l-3.117-5.621c-0.862-1.557-1.812-3.439-2.529-5.146
                              l-0.062,0.02c0.086,1.926,0.128,3.98,0.128,6.357v4.391H165.835L165.835,111.824z"/>
                          </g>
                        </g>
                      </g>
                    </g>
                  </svg>
                
      `
    });

  editor.DomComponents.addType('icon', {
    isComponent: el => el.hasAttribute && el.hasAttribute('data-icon'),
    model: {
      defaults: {
        droppable: false,
        text: `<svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-access-point w-full h-full" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                  <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                  <line x1="12" y1="12" x2="12" y2="12.01"></line>
                  <path d="M14.828 9.172a4 4 0 0 1 0 5.656"></path>
                  <path d="M17.657 6.343a8 8 0 0 1 0 11.314"></path>
                  <path d="M9.168 14.828a4 4 0 0 1 0 -5.656"></path>
                  <path d="M6.337 17.657a8 8 0 0 1 0 -11.314"></path>
              </svg>`,
        traits: [
          {
            name: 'text',
            changeProp: true,
          }]
      },
      init() {
        const comps = this.components();
        const tChild =  comps.length === 1 && comps.models[0];
        const chCnt = (tChild && tChild.is('textnode') && tChild.get('content')) || '';
        const text = chCnt || this.get('text');
        this.set({ text });
        this.on('change:text', this.__onTextChange);
        (text !== chCnt) && this.__onTextChange();
      },

      __onTextChange() {
        this.components(this.get('text'));
      },
    }

  });
  
};