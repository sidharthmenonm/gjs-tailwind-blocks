function apiScript() {

  var el = this;

  function addScript(script_file, script_id, callback) {
    var js, tjs = document.getElementsByTagName('script')[0];
    if (document.getElementById(script_id)) {
      callback();
      return;
    } 
    js = document.createElement('script'); js.id = script_id;
    js.onload = callback;
    js.src = script_file;
    tjs.parentNode.insertBefore(js, tjs);
  }
  
  addScript("/js/embed-script.js", 'api-repeatable-js', function(){});

}

export function loadApiRepeatableBlock (editor, opt = {}) {
  const c = opt;
  let bm = editor.BlockManager;
  const category = { label: 'Advanced', order: 0, open: false };
  
  const toAdd = () => true;

  toAdd('api-repeatable') &&
    bm.add('api-repeatable', {
      id: 'api-repeatable',
      label: 'Api Repeatable',
      category: category,
      content: '<div data-api-repeatable class="p-5"></div>',
    });


  editor.DomComponents.addType('api-repeatable', {
    isComponent: el => el.hasAttribute && el.hasAttribute('data-api-repeatable'),
    model: {
      defaults: {
        traits: [
          'id', 'title',
          
          'data-api', 'data-wrapper-class', 'data-container-class', 'data-image-container-class', 'data-image-class',
          'data-description-container-class', 'data-heading-class', 'data-paragraph-class',
          'data-field', 'data-image', 'data-heading', 'data-paragraph', 'data-link', 'data-link-class', 'data-link-text',
          'data-button-class', 'data-next-button', 'data-prev-button', 'data-next', 'data-prev', 'data-current-page',
          {
            name: 'data-carousal',
            label: "is Carousal",
            type: 'checkbox'
          },
          {
            name: 'data-per-page',
            label: 'Per Page',
            type: 'number'
          },
          {
            name: 'data-autoplay',
            label: "Autoplay",
            type: 'checkbox'
          },
          {
            name: 'data-interval',
            label: 'Interval',
            type: 'number'
          },


        ],
        attributes: {
          'data-carousal-type': 'slide',
          'data-carousal-per-page': 1,
          'data-carousal-per-page-tablet': 1,
          'data-carousal-interval': 5000,
          'data-carousal-arrows': true,
          'data-carousal-pagination': true,
          'data-carousal-loop': true,
          
        },
        script: apiScript,
      },

      init() {
        this.on('change:attributes', this.__onTextChange);
      },

      __onTextChange() {
        this.components('');
        this.updateScript;
      },
    },
    view: {
      init() {
        console.log('init');
        this.listenTo(this.model, 'change:attributes', this.updateScript);
      },
    }
  })


}
