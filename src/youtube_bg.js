import youtube from './data/basic/youtube.svg'

import {getSvgHtml} from './utils'

function youtubeScript(){
  console.log('youtube script');

  if(!this.hasAttribute('data-youtube')) return;

  if(!window.initPlayer){
    window.initPlayer = function(){
      
      video_containers = document.querySelectorAll('[data-youtube]');

      video_containers.forEach(el => {
        
        //check for old player
        oldPlayer = el.player;
        if(oldPlayer){
          oldPlayer && oldPlayer.destroy();
        }

        video_el = el.querySelector('[data-video-bg]').id;
        video_id = el.getAttribute('data-youtube');
        var startSeconds = el.getAttribute('data-start-time');
        var endSeconds = el.getAttribute('data-end-time');

        var isMute = el.hasAttribute('data-mute');
        el.style.backgroundImage = "url(https://img.youtube.com/vi/"+video_id+"/0.jpg)";
        
        var mute_button = el.querySelector('[data-mute-button]');

        function toggelMute() {
          if (el.hasAttribute('data-mute')) {
            //video is muted. set as not muted
            el.removeAttribute('data-mute');
            el.player.unMute();
            mute_button.querySelector('.icon-tabler-volume').classList.remove('hidden')
            mute_button.querySelector('.icon-tabler-volume-3').classList.add('hidden')
          }
          else {
            //video is not muted. set to muted
            el.setAttribute('data-mute', true);
            el.player.mute();
            mute_button.querySelector('.icon-tabler-volume').classList.add('hidden')
            mute_button.querySelector('.icon-tabler-volume-3').classList.remove('hidden')
          }
        }

        if (mute_button) {
          if (isMute) {
            mute_button.querySelector('.icon-tabler-volume').classList.add('hidden')
            mute_button.querySelector('.icon-tabler-volume-3').classList.remove('hidden')
          }
          else {
            mute_button.querySelector('.icon-tabler-volume').classList.remove('hidden')
            mute_button.querySelector('.icon-tabler-volume-3').classList.add('hidden')
          }
          mute_button.removeEventListener('click', toggelMute);
          mute_button.addEventListener('click', toggelMute);
        }

        player = new YT.Player(video_el, {
          videoId: video_id, // YouTube Video ID
          playerVars: {
            autoplay: 1, // Auto-play the video on load
            autohide: 1, // Hide video controls when playing
            disablekb: 1,
            controls: 0, // Hide pause/play buttons in player
            showinfo: 0, // Hide the video title
            modestbranding: 1, // Hide the Youtube Logo
            loop: 1, // Run the video in a loop
            fs: 0, // Hide the full screen button
            rel: 0,
            enablejsapi: 1,
            start: startSeconds,
            end: endSeconds
          },
          events: {
            onReady: function (e) {
              if(isMute) e.target.mute();
              e.target.playVideo();
              document.getElementById(e.target.m.id).classList.add("loaded");
            },
            onStateChange: function (e) {
              if (e.data === YT.PlayerState.PLAYING) {
                document.getElementById(e.target.m.id).classList.add("loaded");
              }
      
              if (e.data === YT.PlayerState.ENDED) {
                // Loop from starting point
                e.target.seekTo(startSeconds);
              }
            }
          }
        });

        el.player = player;

      });

    }
  }

  if(!window.onYouTubeIframeAPIReady){
    window.onYouTubeIframeAPIReady = function () {
      window.initPlayer();
    }
  }

  if(window.youtube_script_loaded){
    window.initPlayer();
  }
  else {
    loadYoutubeApi();
  }

  function loadYoutubeApi(){

    // Add YouTube API script
    var tag = document.createElement("script");
    tag.id = "youtube-script";
    tag.onload = initScript;
    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName("script")[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
  
    function initScript(){
      window.youtube_script_loaded = true;
    }
  
  }

}



export function loadYoutubeBlock (editor, opt = {}) {
  const c = opt;
  let bm = editor.BlockManager;
  const category = { label: 'Advanced', order: 0, open: false };
  
  const toAdd = () => true;

  toAdd('youtube') &&
    bm.add('youtube', {
      id: 'youtube',
      label: getSvgHtml(youtube),
      category: category,
      content: `<div
                  data-start-time="0"
                  data-end-time="187"
                  data-youtube="ybmzgjLnPr0"
                  data-mute
                  class="bg-gray-300 relative overflow-hidden h-96 bg-no-repeat bg-cover bg-center"
                >
                  <div data-video-bg id="youtubebg" class="youtube_video_bg bg-black"></div>
                  <div class="z-20 absolute w-full h-full flex flex-col items-center justify-center text-3xl text-white">
                    <div data-mute-button class="absolute right-4 bottom-4 text-white opacity-50 p-2">
                      <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-volume-3" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                          <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                          <path d="M6 15h-2a1 1 0 0 1 -1 -1v-4a1 1 0 0 1 1 -1h2l3.5 -4.5a0.8 .8 0 0 1 1.5 .5v14a0.8 .8 0 0 1 -1.5 .5l-3.5 -4.5"></path>
                          <path d="M16 10l4 4m0 -4l-4 4"></path>
                      </svg>
                      <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-volume hidden" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                          <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                          <path d="M15 8a5 5 0 0 1 0 8"></path>
                          <path d="M17.7 5a9 9 0 0 1 0 14"></path>
                          <path d="M6 15h-2a1 1 0 0 1 -1 -1v-4a1 1 0 0 1 1 -1h2l3.5 -4.5a0.8 .8 0 0 1 1.5 .5v14a0.8 .8 0 0 1 -1.5 .5l-3.5 -4.5"></path>
                      </svg>
                    </div>
                  </div>
                </div>
      `,

    });

  editor.DomComponents.addType('youtube', {
    isComponent: el => el.hasAttribute && el.hasAttribute('data-youtube'),
    model: {
      defaults: {
        droppable: false,
        traits: [
          'id', 'title', 
          {
            name: 'data-youtube',
            label: "Youtube ID",
            placeholder: "Youtube ID",
          },
          {
            name: 'data-start-time',
            label: 'Start',
            type: 'number'
          },
          {
            name: 'data-end-time',
            label: 'End',
            type: 'number'
          },
          {
            name: 'data-mute',
            label: 'Mute',
            type: 'checkbox',
          }
        ],
        attributes: {
          'data-start-time':  0,
          'data-end-time': 10,
          'data-mute': true,
        },
        script: youtubeScript,
      },
    },
    view: {
      init() {
        this.listenTo(this.model, 
          'change:attributes:data-youtube change:attributes:data-start-time change:attributes:data-end-time change:attributes:data-mute'
          , this.updateScript);
      },
    }
  })

  editor.DomComponents.addType('video-bg', {
    isComponent: el => el.hasAttribute && el.hasAttribute('data-video-bg'),
    model: {
      defaults: {
        droppable: false,
      }
    }
  })

}