import loadBlocks from './src';
import {escapeName} from './src/utils';

import { appendCSS, appendTailwindCss } from './src/utils/appendCss';

import tUIImageEditor from 'grapesjs-tui-image-editor';

var editor = grapesjs.init({
  height: '100%',
  noticeOnUnload: 0,
  storageManager:{autoload: 0},
  container : '#gjs',
  fromElement: true,
  selectorManager: {
    escapeName
  },
  plugins: [
    editor => tUIImageEditor(editor, { /* options */ }),
    'grapesjs-component-code-editor',
    'grapesjs-custom-code'
  ],
  
});

appendTailwindCss(editor, 'https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css');
appendCSS(editor, 'https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.css');
appendCSS(editor, 'https://cdnjs.cloudflare.com/ajax/libs/chartist/0.11.4/chartist.min.css');
appendCSS(editor, "https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/css/splide.min.css");
appendCSS(editor, "https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.3/tiny-slider.css");
appendCSS(editor, "/css/accordion.css");
loadBlocks(editor);

editor.RichTextEditor.remove('link');

editor.RichTextEditor.add('link',
{
    icon: '<span style=\"transform:rotate(45deg)\">&supdsub;</span>',
    attributes: {title: 'Hyperlink'},
    result: rte =>
    {
        var component = editor.getSelected();
        
        if(component.is('link'))
        {
          component.replaceWith(`LINK TEXT`);
        }
        else
        {
            var range = rte.selection().getRangeAt(0);
            
            var container = range.commonAncestorContainer;
            if (container.nodeType == 3)
                container = container.parentNode;
            
            if(container.nodeName === "A")
            {
                var sel = rte.selection();
                sel.removeAllRanges();
                range = document.createRange();
                range.selectNodeContents(container);
                sel.addRange(range);
                rte.exec('unlink');
            }
            else
            {
                var url = window.prompt('Enter the URL to link to:');
                if (url)
                    rte.insertHTML(`<a class="text-blue-400 hover:text-blue-500" href="${url}">${rte.selection()}</a>`);
            }
        }
  }
  });

  editor.RichTextEditor.add('unorderedList',
{
  icon: '&#8226;',
  attributes: {title: 'Unordered List'},
  result: rte => {
    if (window.confirm('Insert List')) rte.exec('insertUnorderedList');
  }
});